//
//  CoreDataManager.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 15/11/22.
//

import CoreData
import Foundation

class CoreDataManager {
    static let shared = CoreDataManager()
    private var persistentContainer: NSPersistentCloudKitContainer
    
    var viewContext: NSManagedObjectContext {
        persistentContainer.viewContext
    }
    
    private init() {
        persistentContainer = NSPersistentCloudKitContainer(name: "CoreDataModel")
        persistentContainer.loadPersistentStores { description, error in
            if let error {
                fatalError("Failed to initialize Core Data stack \(error.localizedDescription)")
            }
            self.persistentContainer.viewContext.automaticallyMergesChangesFromParent = true
        }
    }
    
    func save() {
        if viewContext.hasChanges {
            try? viewContext.save()
        }
    }
}
