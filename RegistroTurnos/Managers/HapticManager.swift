//
//  HapticManager.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 17/12/22.
//

import Foundation
import UIKit

fileprivate final class HapticManager {
    static let shared = HapticManager()
    
    private let feedback = UINotificationFeedbackGenerator()
    
    private init() { }
    
    func trigger(_ notification: UINotificationFeedbackGenerator.FeedbackType) {
        feedback.notificationOccurred(notification)
    }
}

func haptic(_ notification: UINotificationFeedbackGenerator.FeedbackType) {
    HapticManager.shared.trigger(notification)
}
