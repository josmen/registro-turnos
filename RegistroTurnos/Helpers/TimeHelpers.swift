import Foundation

func getDurationInMinutes(from start: String, to end: String) -> Int {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "HH:mm"
    
    let startTime = dateFormatter.date(from: start)!
    let endTime = dateFormatter.date(from: end)!
    
    let calendar = Calendar.current
    let startTimeComponents = calendar.dateComponents([.day, .hour, .minute], from: startTime)
    var endTimeComponents: DateComponents
    if endTime < startTime {
        endTimeComponents = calendar.dateComponents([.day, .hour, .minute], from: endTime.addingTimeInterval(60 * 60 * 24))
    } else {
        endTimeComponents = calendar.dateComponents([.day, .hour, .minute], from: endTime)
    }
    let difference = calendar.dateComponents([.minute], from: startTimeComponents, to: endTimeComponents).minute!
    
    return difference
}

func getHoursAndMinutes(from minutes: Int) -> String {
    let hours = minutes / 60
    let leftMinutes = minutes % 60
    
    if leftMinutes < 10 {
        return "\(hours):0\(leftMinutes)"
    }
    
    return "\(hours)h \(leftMinutes)'"
}

func getCurrentMonth(with quantity: Int = 0) -> Date {
    let calendar = Calendar.current
    
    guard let currentMonth = calendar.date(byAdding: .month, value: quantity, to: Date()) else {
        return Date()
    }
    
    return currentMonth
}

func getCurrentYear(from date: Date) -> Int {
    let calendar = Calendar.current
    
    guard let currentYear = calendar.dateComponents([.year], from: date).year else {
        return 0
    }
    
    return currentYear
}

func isSaturday(_ date: Date) -> Bool {
    let calendar = Calendar(identifier: .gregorian)
    let componentes = calendar.dateComponents([.weekday], from: date)
    return componentes.weekday == 7
}

func isSunday(_ date: Date) -> Bool {
    let calendar = Calendar(identifier: .gregorian)
    let componentes = calendar.dateComponents([.weekday], from: date)
    return componentes.weekday == 1
}

