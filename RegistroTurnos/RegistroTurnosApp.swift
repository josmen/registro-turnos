//
//  RegistroTurnosApp.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 15/11/22.
//

import SwiftUI

@main
struct RegistroTurnosApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, CoreDataManager.shared.viewContext)
                .preferredColorScheme(.dark)
        }
    }
}
