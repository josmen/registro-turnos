//
//  NSLocale+Extensions.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 5/12/22.
//

import Foundation

extension NSLocale {
    @objc
    static let currentLocale = NSLocale(localeIdentifier: "es_ES")
}
