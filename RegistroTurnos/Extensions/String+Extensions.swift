//
//  String-Extensions.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 6/11/22.
//

import Foundation

extension String {
    func asDate() -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter.date(from: self)
    }
}
