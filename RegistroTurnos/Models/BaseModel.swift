//
//  BaseModel.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 15/11/22.
//

import CoreData
import Foundation

protocol BaseModel where Self: NSManagedObject {
    static var viewContext: NSManagedObjectContext { get }
    func save() throws
    func delete() throws
}

extension BaseModel {
    static var viewContext: NSManagedObjectContext {
        return CoreDataManager.shared.viewContext
    }
    
    func save() {
        do {
            try Self.viewContext.save()
            haptic(.success)
        } catch {
            Self.viewContext.rollback()
            print("Error: \(error.localizedDescription)")
            haptic(.error)
        }
    }
    
    func delete() {
        Self.viewContext.delete(self)
        try? save()
    }
}
