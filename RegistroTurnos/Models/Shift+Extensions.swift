//
//  Shift+Extensions.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 15/11/22.
//

import CoreData
import SwiftUI

enum TypeOfShift: String, CaseIterable {
    case morning = "Mañana"
    case noon = "Intermedio"
    case afternoon = "Tarde"
}

extension Shift: BaseModel {
    var shiftName: String {
        name ?? ""
    }
    
    var shiftStartTime: String {
        startTime ?? ""
    }
    
    var shiftEndTime: String {
        endTime ?? ""
    }
    
    var shiftSaturation: Double {
        saturation
    }
    
    var shiftDuration: Int {
        return getDurationInMinutes(from: shiftStartTime, to: shiftEndTime)
    }
    
    var shiftNightMinutes: Int {
        let startTime = shiftStartTime.asDate()!
        let endTime = shiftEndTime.asDate()!
        let nightStartTime = Calendar.current.date(bySettingHour: 22, minute: 0, second: 0, of: startTime)!
        let nightEndTime = Calendar.current.date(bySettingHour: 6, minute: 0, second: 0, of: startTime)!

        if startTime < nightEndTime {
            return Calendar.current.dateComponents([.minute], from: startTime, to: nightEndTime).minute!
        } else if endTime > nightStartTime {
            return Calendar.current.dateComponents([.minute], from: nightStartTime, to: endTime).minute!
        }
        return 0
    }
    
    var typeOfShift: TypeOfShift {
        let startTime = shiftStartTime.asDate()!
        let endTime = shiftEndTime.asDate()!
        let maxMorningStart = Calendar.current.date(bySettingHour: 12, minute: 30, second: 0, of: startTime)!
        let maxMorningEnd = Calendar.current.date(bySettingHour: 15, minute: 45, second: 0, of: startTime)!
        
        if startTime < maxMorningStart && endTime < maxMorningEnd { return .morning }
        else if startTime < maxMorningStart && endTime > maxMorningEnd { return .noon }
        else { return .afternoon }
    }
    
    var shiftColor: Color {
        switch typeOfShift {
        case .morning:
            return .appGreen
        case .noon:
            return .appOrange
        case .afternoon:
            return .appBlue
        }
    }
    
    static func shiftsByDateRequest(_ shiftsGroupDate: ShiftsGroupDate) -> NSFetchRequest<Shift> {
        let request = Shift.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.localizedStandardCompare))]
        request.predicate = NSPredicate(format: "shiftsGroupDate = %@", shiftsGroupDate)
        return request
    }
}
