//
//  Record+Extensions.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 20/11/22.
//

import CoreData
import Foundation

extension Record: BaseModel {
    var recordDate: Date {
        return date ?? Date()
    }
    
    var recordIsSaturday: Bool {
        return isSaturday(recordDate)
    }
    
    var recordIsSunday: Bool {
        return isSunday(recordDate)
    }
    
    var hasExtraData: Bool {
        self.extraTime > 0 ||
        self.isMentoring ||
        self.isSickLeave ||
        self.isWorkedHoliday ||
        self.isSpecialWorkedHoliday ||
        self.isAllowance ||
        self.isFreeLicense ||
        self.isPaidLicense ||
        self.isSPP
    }
    
    var shiftDuration: Int {
        shift?.shiftDuration ?? 0
    }
    
    static var all: NSFetchRequest<Record> {
        let request = Record.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        return request
    }
}
