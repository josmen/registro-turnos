//
//  ShiftsGroupDate+Extensions.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 15/11/22.
//

import CoreData
import Foundation

extension ShiftsGroupDate: BaseModel {
    static var all: NSFetchRequest<ShiftsGroupDate> {
        let request = ShiftsGroupDate.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "validFrom", ascending: false)]
        return request
    }
    
    public var dateValidFrom: Date {
        return self.validFrom ?? Date()
    }
    
    public var dateShifts: [Shift] {
        let set = shifts as? Set<Shift> ?? []
        return set.sorted {
            $0.name!.localizedStandardCompare($1.name!) == .orderedAscending
        }
    }
}
