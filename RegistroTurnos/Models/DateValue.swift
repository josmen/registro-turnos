//
//  DateValue.swift
//  ElegantTaskApp
//
//  Created by Jose Antonio Mendoza on 2/12/22.
//

import SwiftUI

struct DateValue: Identifiable {
    var id = UUID().uuidString
    var day: Int
    var date: Date
}
