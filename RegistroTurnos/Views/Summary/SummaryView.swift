//
//  SummaryView.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 29/11/22.
//

import SwiftUI

struct SummaryView: View {
    @FetchRequest(fetchRequest: Record.all)
    private var records: FetchedResults<Record>
    
    @Binding var currentDate: Date
    @Binding var currentMonth: Int
    
    @State private var showPayrollData: Bool = true
    @State private var showYearData: Bool = false
    @State private var showMonthData: Bool = false
    
    private let hoursToWork: Double = 1567
    private let allowanceValue: Double = 0.95
    
    var body: some View {
        VStack {
            MonthYearHeader(currentDate: $currentDate, currentMonth: $currentMonth)
            
            ScrollView {
                VStack(spacing: 20) {
                    PayrollGroupBox
                    MonthGroupBox
                    YearGroupBox
                }
                .tint(.white)
            }
            .padding()
            .onChange(of: currentMonth) { _, newMonth in
                currentDate = getCurrentMonth(with: newMonth)
            }
            .scrollIndicators(.hidden)
        }
        .padding(.vertical)
    }
}

// MARK: - Extracted views
extension SummaryView {
    var PayrollGroupBox: some View {
        DisclosureGroup(isExpanded: $showPayrollData) {
            LabeledContent("Nocturnidad", value: nightTimeInMonth, format: .number)
            LabeledContent("Comp. Jor. Cont. Peculiares", value: noonRecordsCount, format: .number)
            LabeledContent("Prima saturación maquinistas", value: saturationInMonth, format: .number)
            LabeledContent("Indemnización Domingo/Festivo", value: numberOfWorkedHolidaysAndSundays, format: .number)
            LabeledContent("Indemnización descanso bocadillo", value: snackBreakCompensation, format: .number)
            LabeledContent("Indemnización sábados", value: numberOfSaturdays, format: .number)
            LabeledContent("Horas extras extructurales", value: extraTimeInMonth, format: .number.precision(.fractionLength(2)))
            LabeledContent("SPP", value: totalSPPHours, format: .number.precision(.fractionLength(2)))
            LabeledContent("Dietas", value: numberOfAllowance, format: .number.precision(.fractionLength(2)))
            LabeledContent("Comp. Festivos Especiales", value: numberOfSpecialWorkedHolidays, format: .number)
        } label: {
            Label("Nómina \(currentDate.format("MMM yyyy"))", systemImage: "doc.text.magnifyingglass")
                .font(.title3.bold())
        }
    }
    
    var YearGroupBox: some View {
        DisclosureGroup(isExpanded: $showYearData) {
            LabeledContent("Horas trabajadas", value: totalWorkedHours, format: .number.precision(.fractionLength(2)))
            LabeledContent("Días trabajados", value: workedDaysCount, format: .number)
            ForEach(TypeOfShift.allCases, id: \.self) { typeOfShift in
                let (hours, days) = recordsByType(recordsInYear, typeOfShift)
                LabeledContent("\(typeOfShift.rawValue) (\(days))", value: hours, format: .number.precision(.fractionLength(2)))
            }
        } label: {
            Text("Datos de \(currentDate.format("yyyy"))")
                .font(.title3.bold())
        }
    }
    
    var MonthGroupBox: some View {
        DisclosureGroup(isExpanded: $showMonthData) {
            LabeledContent("Horas trabajadas", value: monthWorkedHours, format: .number.precision(.fractionLength(2)))
            LabeledContent("Días trabajados", value: workedDaysInCurrentMonth, format: .number)
            ForEach(TypeOfShift.allCases, id: \.self) { typeOfShift in
                let (hours, days) = recordsByType(recordsInMonth, typeOfShift)
                LabeledContent("\(typeOfShift.rawValue) (\(days))", value: hours, format: .number.precision(.fractionLength(2)))
            }
        } label: {
            Text("Datos de \(currentDate.format("MMMM"))")
                .font(.title3.bold())
        }
    }
}

// MARK: Computed properties and functions
extension SummaryView {
    func recordsByType(_ records: [Record], _ typeOfShift: TypeOfShift) -> (hours: Double, days: Int) {
        let records = records.filter { $0.shift?.typeOfShift == typeOfShift }
        let minutes = records.reduce(0) { $0 + $1.shiftDuration }
        let hours = Double(minutes) / 60
        return (hours, records.count)
    }
    // Year
    func recordsByYear(_ date: Date) -> [Record] {
        let calendar = Calendar.current
        let currentYear = calendar.dateComponents([.year], from: date)
        let filteredRecords = records.filter { record in
            calendar.dateComponents([.year], from: record.recordDate) == currentYear &&
            record.isSPP == false
        }
        return filteredRecords
    }
    
    var recordsInYear: [Record] {
        recordsByYear(self.currentDate)
    }
    
    var workedDaysCount: Int {
        recordsInYear.count
    }
    
    var totalWorkedHours: Double {
        let totalMinutes = recordsInYear.reduce(0) { partialResult, record in
            return partialResult + (record.shift?.shiftDuration ?? 0)
        }
        return Double(totalMinutes) / 60
    }
    
    func recordsInYearByType(_ typeOfShift: TypeOfShift) -> [Record] {
        recordsInYear.filter { $0.shift?.typeOfShift == typeOfShift }
    }
    
    func countOfRecordsInYearByType(_ typeOfShift: TypeOfShift) -> Int {
        recordsInYearByType(typeOfShift).count
    }
    
    func totalWorkedHoursByType(_ typeOfShift: TypeOfShift) -> Double {
        let records = recordsInYearByType(typeOfShift)
        let minutes = records.reduce(0) { $0 + $1.shiftDuration }
        return Double(minutes) / 60
    }
    
    // Month
    func recordsByMonth(_ date: Date, filterSpp: Bool = false) -> [Record] {
        let calendar = Calendar.current
        let currentMonth = calendar.dateComponents([.month, .year], from: date)
        let filteredRecords = records.filter { record in
            calendar.dateComponents([.month, .year], from: record.recordDate) == currentMonth &&
            record.isSPP == filterSpp
        }
        return filteredRecords
    }

    var recordsInMonth: [Record] {
        recordsByMonth(currentDate)
    }
    
    var notSickRecordsInMonth: [Record] {
        recordsInMonth.filter { $0.isSickLeave == false && $0.shift?.name != "STDR" }
    }
    
    var workedDaysInCurrentMonth: Int {
        recordsInMonth.count
    }
    
    var snackBreakCompensation: Int {
        notSickRecordsInMonth.count
    }
    
    var monthWorkedHours: Double {
        let minutesInMonth = recordsInMonth.reduce(0) { $0 + $1.shiftDuration }
        return Double(minutesInMonth) / 60
    }
    
    var totalSPPHours: Double {
        let totalMinutes = sppRecordsInMonth.reduce(0) { partialResult, record in
            return partialResult + (record.shift?.shiftDuration ?? 0)
        }
        return Double(totalMinutes) / 60
    }
    
    var sppRecordsInMonth: [Record] {
        recordsByMonth(currentDate, filterSpp: true)
    }
    
    var nightTimeInMonth: Double {
        let totalMinutes = notSickRecordsInMonth.reduce(0) { partialResult, record in
            return partialResult + (record.shift?.shiftNightMinutes)!
        }
        return (Double(totalMinutes) / 60).rounded()
    }
    
    var noonRecordsCount: Int {
        notSickRecordsInMonth.filter { $0.shift?.typeOfShift == .noon }.count
    }
    
    var extraTimeInMonth: Double {
        let totalMinutes = recordsInMonth.reduce(0) { partialResult, record in
            return partialResult + record.extraTime
        }
        return Double(totalMinutes) / 60
    }
    
    var saturationInMonth: Double {
        let totalSaturation = notSickRecordsInMonth.reduce(0.0) { partialResult, record in
            guard let saturation = record.shift?.saturation else { return partialResult }
            return partialResult + saturation
        }
        return totalSaturation
    }
    
    var numberOfSaturdays: Int {
        notSickRecordsInMonth.filter { $0.recordIsSaturday }.count
    }
    
    var numberOfWorkedHolidaysAndSundays: Int {
        notSickRecordsInMonth.filter { $0.recordIsSunday || $0.isWorkedHoliday }.count
    }
    
    var numberOfAllowance: Double {
        let allowanceDays = recordsInMonth.filter { $0.isAllowance }.count
        return Double(allowanceDays) * allowanceValue
    }
    
    var numberOfSpecialWorkedHolidays: Int {
        notSickRecordsInMonth.filter { $0.isSpecialWorkedHoliday }.count
    }
}
