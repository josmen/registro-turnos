//
//  AddShiftsGroupDateView.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 15/11/22.
//

import SwiftUI

struct AddShiftsGroupDateView: View {
    @Environment(\.managedObjectContext) private var viewContext
    @Environment(\.dismiss) private var dismiss
    @State private var validFrom: Date = Date()
    @State private var showingDeleteConfirm: Bool = false
    
    private var shiftsGroupDate: ShiftsGroupDate?
    
    init(shiftsGroupDate: ShiftsGroupDate? = nil) {
        self.shiftsGroupDate = shiftsGroupDate
    }
    
    var body: some View {
        NavigationStack {
            Form {
                Section {
                    DatePicker("Válido desde", selection: $validFrom, displayedComponents: .date)
                } header: {
                    Text("Fecha de los nuevos turnos")
                }
                
                Section {
                    Button(isEditing ? "Actualizar" : "Guardar") {
                        saveOrUpdate()
                    }
                    if isEditing {
                        Button("Borrar horarios", role: .destructive) {
                            showingDeleteConfirm = true
                        }
                        .alert("Borrar horarios", isPresented: $showingDeleteConfirm) {
                            Button("Borrar", role: .destructive, action: delete)
                        } message: {
                            Text("¿Estás seguro de borrar todos los horarios válidos desde \(validFrom.asFormattedDateString())?")
                        }

                    }
                }
            }
            .navigationTitle(isEditing ? "Actualizar fecha" : "Nuevos turnos")
            .onAppear {
                if let shiftsGroupDate {
                    validFrom = shiftsGroupDate.dateValidFrom
                }
            }
        }
    }
}

extension AddShiftsGroupDateView {
    private var isEditing: Bool {
        shiftsGroupDate != nil
    }
    
    private func saveOrUpdate() {
        if let shiftsGroupDate {
            shiftsGroupDate.validFrom = validFrom
            shiftsGroupDate.save()
        } else {
            let shiftsGroupDate = ShiftsGroupDate(context: viewContext)
            shiftsGroupDate.validFrom = validFrom
            shiftsGroupDate.save()
        }
        dismiss()
    }
    
    private func delete() {
        shiftsGroupDate?.delete()
        dismiss()
    }
}
