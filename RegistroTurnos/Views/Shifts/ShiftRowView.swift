//
//  ShiftCellView.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 17/11/22.
//

import SwiftUI

struct ShiftRowView: View {
    let shift: Shift
    
    var body: some View {
        HStack {
            ShiftCapsuleView(shift: shift)
            Spacer()
            Label(shift.shiftStartTime, systemImage: "arrow.up.right")
                .foregroundColor(.secondary)
            Label(shift.shiftEndTime, systemImage: "arrow.down.right")
                .foregroundColor(.secondary)
        }
        .padding(.horizontal)
    }
}
