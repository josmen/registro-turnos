//
//  ShiftListView.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 15/11/22.
//

import CoreData
import SwiftUI

struct ShiftListView: View {
    @FetchRequest(fetchRequest: ShiftsGroupDate.all) var allShiftsGroupDates
    @State private var isPresented: Bool = false
    
    var body: some View {
        NavigationStack {
            customHeader()
            
            List {
                if allShiftsGroupDates.isEmpty {
                    Text("No hay horarios")
                        .listRowSeparator(.hidden)
                } else {
                    ForEach(allShiftsGroupDates) { shiftsGroupDate in
                        Section {
                            ShiftsByDateView(request: Shift.shiftsByDateRequest(shiftsGroupDate))
                            
                            NavigationLink {
                                AddShiftView(shiftsGroupDate: shiftsGroupDate)
                            } label: {
                                HStack {
                                    Image(systemName: "plus.circle")
                                    Text("Añadir turno")
                                }
                                .font(.title2)
                                .foregroundColor(.accentColor)
                            }
                        } header: {
                            HStack {
                                Text("Vigentes desde \(shiftsGroupDate.dateValidFrom.asFormattedDateString())")
                                Spacer()
                                NavigationLink {
                                    AddShiftsGroupDateView(shiftsGroupDate: shiftsGroupDate)
                                } label: {
                                    Image(systemName: "square.and.pencil")
                                        .imageScale(.large)
                                }
                            }
                        }
                    }
                    .listRowSeparator(.hidden)
                }
            }
            .listStyle(.plain)
            .sheet(isPresented: $isPresented) {
                AddShiftsGroupDateView()
                    .presentationDetents([.medium])
            }
        }
    }
}

extension ShiftListView {
    @ViewBuilder
    func customHeader() -> some View {
        HStack(spacing: 20) {
            VStack(alignment: .leading, spacing: 10) {
                Text("Horarios").font(.title.bold())
            }
            Spacer(minLength: 0)
            ButtonView(icon: "plus.circle") {
                isPresented = true
            }
        }
        .padding(.horizontal)
        .padding(.top, 16)
    }
}
