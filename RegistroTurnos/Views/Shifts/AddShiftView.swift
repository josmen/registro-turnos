//
//  AddShiftView.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 16/11/22.
//

import SwiftUI

struct AddShiftView: View {
    @Environment(\.dismiss) private var dismiss
    @Environment(\.managedObjectContext) var viewContext
    
    @State private var name: String = ""
    @State private var startTime: Date = Date()
    @State private var endTime: Date = Date()
    @State private var saturation: Double = 0.0
    
    var shiftsGroupDate: ShiftsGroupDate
    
    init(shiftsGroupDate: ShiftsGroupDate) {
        self.shiftsGroupDate = shiftsGroupDate
    }
    
    var body: some View {
        NavigationStack {
            Form {
                Section {
                    TextField("Turno", text: $name)
                    DatePicker("Inicio", selection: $startTime, displayedComponents: .hourAndMinute)
                    DatePicker("Fin", selection: $endTime, displayedComponents: .hourAndMinute)
                } header: {
                    Text("Horario del nuevo turno")
                }
                
                Section {
                    TextField("Saturación", value: $saturation, format: .number)
                        .keyboardType(.decimalPad)
                } header: {
                    Text("Saturación")
                }
                
                Section {
                    Button("Guardar") {
                        save()
                    }
                    .disabled(!isFormValid)
                }
            }
            .navigationTitle("Nuevo turno")
        }
    }
}

extension AddShiftView {
    private var isFormValid: Bool {
        return !name.isEmpty
    }
    
    private func save() {
        let shift = Shift(context: viewContext)
        shift.name = name
        shift.startTime = startTime.asFormattedTimeString()
        shift.endTime = endTime.asFormattedTimeString()
        
        shiftsGroupDate.addToShifts(shift)
        shiftsGroupDate.save()
        dismiss()
    }
}
