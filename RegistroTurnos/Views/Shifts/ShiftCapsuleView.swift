//
//  ShiftCapsule.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 2/12/22.
//

import SwiftUI

struct ShiftCapsuleView: View {
    let shift: Shift
    
    var body: some View {
        Text(shift.shiftName)
            .font(.headline)
            .fontWeight(.black)
            .frame(minWidth: 35, maxHeight: 25)
            .padding(6)
            .background(shift.shiftColor)
            .clipShape(Capsule())
            .foregroundColor(.white)
    }
}
