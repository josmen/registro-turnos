//
//  EditShiftView.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 3/12/22.
//

import SwiftUI

struct EditShiftView: View {
    @Environment(\.dismiss) private var dismiss
    @Environment(\.managedObjectContext) var viewContext
    
    @Binding var refreshingID: UUID
    
    @State private var name: String = ""
    @State private var startTime: Date = Date()
    @State private var endTime: Date = Date()
    @State private var saturation: Double = 0.0
    
    var shift: Shift
    
    var body: some View {
        NavigationStack {
            Form {
                Section {
                    TextField("Turno", text: $name).keyboardType(.numberPad)
                    DatePicker("Inicio", selection: $startTime, displayedComponents: .hourAndMinute)
                    DatePicker("Fin", selection: $endTime, displayedComponents: .hourAndMinute)
                } header: {
                    Text("Horario del turno")
                }
                
                Section {
                    TextField("Saturación", value: $saturation, format: .number)
                        .keyboardType(.decimalPad)
                } header: {
                    Text("Saturación")
                }
                
                Section {
                    Button("Actualizar") {
                        update()
                    }
                    .disabled(!isFormValid)
                    
                    Button("Borrar turno", role: .destructive, action: deleteShift)
                }
                
            }
            .navigationTitle("Actualizar turno")
            .onAppear {
                self.name = shift.shiftName
                self.startTime = shift.startTime?.asDate() ?? Date()
                self.endTime = shift.endTime?.asDate() ?? Date()
                self.saturation = shift.shiftSaturation
            }
        }
    }
}

extension EditShiftView {
    private var isFormValid: Bool {
        return !name.isEmpty
    }
    
    private func update() {
        shift.name = name
        shift.startTime = startTime.asFormattedTimeString()
        shift.endTime = endTime.asFormattedTimeString()
        shift.saturation = saturation
        shift.save()
        self.refreshingID = UUID()
        dismiss()
    }
    
    private func deleteShift() {
        shift.delete()
        dismiss()
    }
}
