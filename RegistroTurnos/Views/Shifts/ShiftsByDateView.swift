//
//  ShiftsByDateView.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 16/11/22.
//

import CoreData
import SwiftUI

struct ShiftsByDateView: View {
    @State private var refreshingID = UUID()
    @FetchRequest var shifts: FetchedResults<Shift>
    
    init(request: NSFetchRequest<Shift>) {
        _shifts = FetchRequest(fetchRequest: request)
    }
    
    var body: some View {
        if shifts.isEmpty {
            Text("No hay ningún horario")
        } else {
            ForEach(shifts) { shift in
                NavigationLink {
                    EditShiftView(refreshingID: $refreshingID, shift: shift)
                } label: {
                    ShiftRowView(shift: shift)
                }
            }
            .id(refreshingID)
        }
    }
}

extension ShiftsByDateView {
    private func deleteShift(at indexSet: IndexSet) {
        indexSet.forEach { index in
            let shift = shifts[index]
            shift.delete()
        }
    }
}
