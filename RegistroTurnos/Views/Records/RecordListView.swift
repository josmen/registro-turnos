//
//  RecordListView.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 15/11/22.
//

import CoreData
import SwiftUI

struct RecordListView: View {
    @Environment(\.managedObjectContext) private var viewContext
    
    @FetchRequest(fetchRequest: Record.all)
    private var records: FetchedResults<Record>
    
    var yearDictionary: [String: [Record]] {
        Dictionary(grouping: records, by: { $0.date?.month ?? "" })
    }
    
    var body: some View {
        NavigationStack {
            List {
                if records.isEmpty {
                    Text("No hay registros")
                } else {
                    
                    let formatter: DateFormatter = {
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "MMMM yyyy"
                        return dateFormatter
                    }()
                    
                    ForEach(yearDictionary.keys.sorted(by: { formatter.date(from: $0)! > formatter.date(from: $1)! }), id: \.self) { month in
                        Section {
                            ForEach(yearDictionary[month]!) { record in
                                NavigationLink {
                                    EditRecordView(moc: viewContext, record: record)
                                } label: {
                                    RecordRowView(record: record)
                                }
                            }
                            .listRowSeparator(.hidden)
                        } header: {
                            Text(month.capitalized)
                        }
                        .headerProminence(.increased)
                    }
                }
            }
            .listStyle(.plain)
            .navigationTitle("Registro de turnos")
        }
    }
}

extension RecordListView {
    private func showRecord(_ record: Record) {
        print("""
        Record:
            - fecha: \(record.recordDate.asFormattedDateString())
            - exceso: \(record.extraTime)
            - turno: \(record.shift?.name  ?? "")
            - inicio: \(record.shift?.startTime ?? "")
            - fin: \(record.shift?.endTime ?? "")
            - id del turno: \(String(describing: record.shift?.objectID))
        
        """)
    }
}
