//
//  RecordRowView.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 3/12/22.
//

import SwiftUI

struct RecordRowView: View {
    let record: Record
    
    var body: some View {
        HStack {
            ShiftCapsuleView(shift: record.shift!)
            if record.extraTime > 0 {
                Text("E")
                    .font(.caption.bold())
                    .frame(width: 20)
                    .padding(4)
                    .background(.purple)
                    .clipShape(Circle())
                    .foregroundColor(.white)
            }
            Spacer()
            Text(record.recordDate.asFormattedDateString())
                .monospacedDigit()
        }
        .padding(.horizontal)
    }
}
