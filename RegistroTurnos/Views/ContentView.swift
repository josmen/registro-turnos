//
//  ContentView.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 15/11/22.
//

import SwiftUI

struct ContentView: View {
    @Environment(\.managedObjectContext) var viewContext

    @FetchRequest(fetchRequest: ShiftsGroupDate.all)
    private var currentShifts: FetchedResults<ShiftsGroupDate>
    
    @State private var isPresented: Bool = false
    @State private var currentDate = Date()
    
    // Month update on arrow button clicks
    @State private var currentMonth: Int = 0
    
    var body: some View {
            if currentShifts.isEmpty {
                VStack {
                    Text("No hay datos todavía")
                    Button {
                        isPresented = true
                    } label: {
                        Text("Empieza creando nuevos horarios")
                    }
                    .buttonStyle(.bordered)
//                    Button {
//                        createData_20210114()
//                        createData_20220119()
//                        createData_20220713()
//                    } label: {
//                        Text("Pulsa aquí para crear horarios")
//                    }
//                    .buttonStyle(.borderedProminent)
                }
                .sheet(isPresented: $isPresented) {
                    ShiftListView()
                }
            } else {
                TabView {
                    CalendarView(currentDate: $currentDate, currentMonth: $currentMonth)
                        .tabItem {
                            Label("Calendario", systemImage: "calendar")
                        }
//                    RecordListView()
//                        .tabItem {
//                            Label("Registro", systemImage: "list.bullet")
//                        }
                    ShiftListView()
                        .tabItem {
                            Label("Horarios", systemImage: "clock")
                        }
                    SummaryView(currentDate: $currentDate, currentMonth: $currentMonth)
                        .tabItem {
                            Label("Resumen", systemImage: "chart.bar.xaxis")
                        }
                }
            }
            
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let viewContext = CoreDataManager.shared.viewContext
        ContentView()
            .environment(\.managedObjectContext, viewContext)
    }
}

extension ContentView {
    
    private func createData_20210114() {
        let newShiftsGroup = ShiftsGroupDate(context: viewContext)
        newShiftsGroup.validFrom = Date(2021, 01, 14)
        
        let newShift1 = Shift(context: viewContext)
        newShift1.name = "1"
        newShift1.startTime = "04:40"
        newShift1.endTime = "11:45"
        newShiftsGroup.addToShifts(newShift1)
        
        let newShift2 = Shift(context: viewContext)
        newShift2.name = "2"
        newShift2.startTime = "05:26"
        newShift2.endTime = "13:45"
        newShiftsGroup.addToShifts(newShift2)
        
        let newShift3 = Shift(context: viewContext)
        newShift3.name = "3"
        newShift3.startTime = "06:26"
        newShift3.endTime = "14:45"
        newShiftsGroup.addToShifts(newShift3)
        
        let newShift4 = Shift(context: viewContext)
        newShift4.name = "4"
        newShift4.startTime = "11:38"
        newShift4.endTime = "18:45"
        newShiftsGroup.addToShifts(newShift4)
        
        let newShift5 = Shift(context: viewContext)
        newShift5.name = "5"
        newShift5.startTime = "13:38"
        newShift5.endTime = "22:52"
        newShiftsGroup.addToShifts(newShift5)
        
        let newShift6 = Shift(context: viewContext)
        newShift6.name = "6"
        newShift6.startTime = "14:38"
        newShift6.endTime = "23:16"
        newShiftsGroup.addToShifts(newShift6)
        
        let newShift7 = Shift(context: viewContext)
        newShift7.name = "7"
        newShift7.startTime = "16:38"
        newShift7.endTime = "23:36"
        newShiftsGroup.addToShifts(newShift7)
        
        let newShift8 = Shift(context: viewContext)
        newShift8.name = "8"
        newShift8.startTime = "05:00"
        newShift8.endTime = "13:30"
        newShiftsGroup.addToShifts(newShift8)
        
        let newShift9 = Shift(context: viewContext)
        newShift9.name = "9"
        newShift9.startTime = "13:00"
        newShift9.endTime = "21:15"
        newShiftsGroup.addToShifts(newShift9)
        
        newShiftsGroup.save()
    }
    
    private func createData_20220119() {
        let newShiftsGroup = ShiftsGroupDate(context: viewContext)
        newShiftsGroup.validFrom = Date(2022, 01, 19)
        
        let newShift1 = Shift(context: viewContext)
        newShift1.name = "1"
        newShift1.startTime = "04:40"
        newShift1.endTime = "11:37"
        newShiftsGroup.addToShifts(newShift1)
        
        let newShift2 = Shift(context: viewContext)
        newShift2.name = "2"
        newShift2.startTime = "05:26"
        newShift2.endTime = "13:37"
        newShiftsGroup.addToShifts(newShift2)
        
        let newShift3 = Shift(context: viewContext)
        newShift3.name = "3"
        newShift3.startTime = "06:26"
        newShift3.endTime = "14:37"
        newShiftsGroup.addToShifts(newShift3)
        
        let newShift4 = Shift(context: viewContext)
        newShift4.name = "4"
        newShift4.startTime = "11:51"
        newShift4.endTime = "18:37"
        newShiftsGroup.addToShifts(newShift4)
        
        let newShift5 = Shift(context: viewContext)
        newShift5.name = "5"
        newShift5.startTime = "13:51"
        newShift5.endTime = "22:52"
        newShiftsGroup.addToShifts(newShift5)
        
        let newShift6 = Shift(context: viewContext)
        newShift6.name = "6"
        newShift6.startTime = "14:51"
        newShift6.endTime = "23:16"
        newShiftsGroup.addToShifts(newShift6)
        
        let newShift7 = Shift(context: viewContext)
        newShift7.name = "7"
        newShift7.startTime = "16:51"
        newShift7.endTime = "23:36"
        newShiftsGroup.addToShifts(newShift7)
        
        let newShift8 = Shift(context: viewContext)
        newShift8.name = "8"
        newShift8.startTime = "05:00"
        newShift8.endTime = "13:20"
        newShiftsGroup.addToShifts(newShift8)
        
        let newShift9 = Shift(context: viewContext)
        newShift9.name = "9"
        newShift9.startTime = "13:50"
        newShift9.endTime = "21:15"
        newShiftsGroup.addToShifts(newShift9)
        
        let newShift10 = Shift(context: viewContext)
        newShift10.name = "STDR"
        newShift10.startTime = "07:00"
        newShift10.endTime = "14:47"
        newShiftsGroup.addToShifts(newShift10)
        
        newShiftsGroup.save()
    }
    
    private func createData_20220713() {
        let newShiftsGroup = ShiftsGroupDate(context: viewContext)
        newShiftsGroup.validFrom = Date(2022, 07, 13)
        
        let newShift1 = Shift(context: viewContext)
        newShift1.name = "1"
        newShift1.startTime = "04:27"
        newShift1.endTime = "11:36"
        newShiftsGroup.addToShifts(newShift1)
        
        let newShift2 = Shift(context: viewContext)
        newShift2.name = "2"
        newShift2.startTime = "05:27"
        newShift2.endTime = "12:36"
        newShiftsGroup.addToShifts(newShift2)
        
        let newShift3 = Shift(context: viewContext)
        newShift3.name = "3"
        newShift3.startTime = "06:27"
        newShift3.endTime = "13:36"
        newShiftsGroup.addToShifts(newShift3)
        
        let newShift4 = Shift(context: viewContext)
        newShift4.name = "4"
        newShift4.startTime = "07:52"
        newShift4.endTime = "14:36"
        newShiftsGroup.addToShifts(newShift4)
        
        let newShift5 = Shift(context: viewContext)
        newShift5.name = "5"
        newShift5.startTime = "12:52"
        newShift5.endTime = "19:36"
        newShiftsGroup.addToShifts(newShift5)
        
        let newShift6 = Shift(context: viewContext)
        newShift6.name = "6"
        newShift6.startTime = "13:52"
        newShift6.endTime = "22:51"
        newShiftsGroup.addToShifts(newShift6)
        
        let newShift7 = Shift(context: viewContext)
        newShift7.name = "7"
        newShift7.startTime = "14:52"
        newShift7.endTime = "23:15"
        newShiftsGroup.addToShifts(newShift7)
        
        let newShift8 = Shift(context: viewContext)
        newShift8.name = "8"
        newShift8.startTime = "15:52"
        newShift8.endTime = "23:51"
        newShiftsGroup.addToShifts(newShift8)
        
        let newShift9 = Shift(context: viewContext)
        newShift9.name = "9"
        newShift9.startTime = "04:45"
        newShift9.endTime = "12:45"
        newShiftsGroup.addToShifts(newShift9)
        
        let newShift10 = Shift(context: viewContext)
        newShift10.name = "10"
        newShift10.startTime = "14:00"
        newShift10.endTime = "21:00"
        newShiftsGroup.addToShifts(newShift10)
        
        let newShift11 = Shift(context: viewContext)
        newShift11.name = "STDR"
        newShift11.startTime = "07:00"
        newShift11.endTime = "14:47"
        newShiftsGroup.addToShifts(newShift11)
        
        newShiftsGroup.save()
    }
}
