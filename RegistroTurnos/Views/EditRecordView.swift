//
//  EditRecordView.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 20/11/22.
//

import CoreData
import SwiftUI

enum LicenseType: String, CaseIterable {
    case paid = "Con sueldo"
    case free = "Sin sueldo"
}

struct EditRecordView: View {
    @Environment(\.dismiss) private var dismiss
    @Environment(\.managedObjectContext) private var viewContext
    @FetchRequest private var allShiftsGroupDates: FetchedResults<ShiftsGroupDate>
    
    // TODO: crear un ViewModel
    @State private var shifts: [Shift]
    @State private var selectedShift: Shift
    @State private var date: Date
    @State private var showingDeleteConfirm: Bool = false
    @State private var showingExtra: Bool
    @State private var extraTime: Double
    @State private var isMentoring: Bool
    @State private var isSickLeave: Bool
    @State private var isWorkedHoliday: Bool
    @State private var isSpecialWorkedHoliday: Bool
    @State private var isAllowance: Bool
    @State private var isSPP: Bool
    @State private var isLicense: Bool
    @State private var licenseType: LicenseType = .paid

    var record: Record
    
    init(moc: NSManagedObjectContext, record: Record) {
        let fetchRequest: NSFetchRequest<ShiftsGroupDate> = ShiftsGroupDate.all
        self._allShiftsGroupDates = FetchRequest(fetchRequest: fetchRequest)
        
        do {
            let allShiftsGroups = try moc.fetch(fetchRequest)
            self.record = record
            
            if let shift = record.shift {
                let currentShifts = allShiftsGroups.filter { $0.dateShifts.contains(shift) }.first
                self._shifts = State(initialValue: currentShifts?.dateShifts ?? [])
                self._selectedShift = State(initialValue: record.shift!)
                self._date = State(initialValue: record.date!)
                self._showingExtra = (State(initialValue: record.hasExtraData))
                self._extraTime = State(initialValue: Double(record.extraTime))
                self._isMentoring = State(initialValue: record.isMentoring)
                self._isSickLeave = State(initialValue: record.isSickLeave)
                self._isWorkedHoliday = State(initialValue: record.isWorkedHoliday)
                self._isSpecialWorkedHoliday = State(initialValue: record.isSpecialWorkedHoliday)
                self._isAllowance = State(initialValue: record.isAllowance)
                self._isSPP = State(initialValue: record.isSPP)
                self._isLicense = State(initialValue: record.isPaidLicense || record.isFreeLicense)
                self._licenseType = State(initialValue: record.isPaidLicense ? .paid : .free)
            } else {
                let currentShifts = allShiftsGroups[0]
                self._shifts = State(initialValue: currentShifts.dateShifts)
                self._selectedShift = State(initialValue: currentShifts.dateShifts.first!)
                self._date = State(initialValue: Date())
                self._showingExtra = (State(initialValue: false))
                self._extraTime = State(initialValue: 0)
                self._isMentoring = State(initialValue: false)
                self._isSickLeave = State(initialValue: false)
                self._isWorkedHoliday = State(initialValue: false)
                self._isSpecialWorkedHoliday = State(initialValue: false)
                self._isAllowance = State(initialValue: false)
                self._isSPP = State(initialValue: false)
                self._isLicense = State(initialValue: false)
                self._licenseType = State(initialValue: .paid)
            }
        } catch {
            fatalError("Error while fetching shifts")
        }
    }
    
    var body: some View {
        NavigationStack {
            Form {
                Section {
                    DatePicker("Fecha", selection: $date, displayedComponents: .date)
                        .onChange(of: date) {
                            refreshShifts()
                        }
                    
                    Picker("Turno", selection: $selectedShift) {
                        ForEach(shifts, id: \.self) { shift in
                            Text(shift.shiftName)
                                .tag(shift)
                        }
                    }
                    
                    HStack {
                        Label(selectedShift.shiftStartTime, systemImage: "arrow.up.right")
                        Spacer()
                        Label(selectedShift.shiftEndTime, systemImage: "arrow.down.right")
                    }
                    .foregroundColor(.secondary)
                }
                
                Section {
                    Toggle("Otros datos", isOn: $showingExtra.animation(.spring()))
                        .toggleStyle(SwitchToggleStyle(tint: .appPurple))
                }
                    
                if showingExtra {
                    Section("Exceso de jornada") {
                        Text("\(extraTime.formatted()) minutos")
                        Slider(value: $extraTime, in: 0...150, step: 1)
                    }
                    
                    Section {
                        Toggle("Día festivo", isOn: $isWorkedHoliday)
                            .toggleStyle(SwitchToggleStyle(tint: .appPurple))
                        Toggle("Festivo especial", isOn: $isSpecialWorkedHoliday)
                            .toggleStyle(SwitchToggleStyle(tint: .appPurple))
                        Toggle("Práctica", isOn: $isMentoring)
                            .toggleStyle(SwitchToggleStyle(tint: .appPurple))
                        Toggle("Baja por enfermedad", isOn: $isSickLeave)
                            .toggleStyle(SwitchToggleStyle(tint: .appPurple))
                        Toggle("Dieta", isOn: $isAllowance.animation(.spring()))
                            .toggleStyle(SwitchToggleStyle(tint: .appPurple))
                        Toggle("SPP", isOn: $isSPP.animation(.spring()))
                            .toggleStyle(SwitchToggleStyle(tint: .appPurple))
                        Toggle("Licencia", isOn: $isLicense.animation(.spring()))
                            .toggleStyle(SwitchToggleStyle(tint: .appPurple))
                        if isLicense {
                            Picker("", selection: $licenseType) {
                                ForEach(LicenseType.allCases, id: \.self) { license in
                                    Text(license.rawValue).tag(license)
                                }
                            }
                            .pickerStyle(.segmented)
                        }
                    }
                }
                
                Button("Borrar turno", role: .destructive) {
                    showingDeleteConfirm = true
                }
                .alert("Borrar turno", isPresented: $showingDeleteConfirm) {
                    Button("Borrar", role: .destructive, action: deleteRecord)
                } message: {
                    Text("¿Estás seguro de borrar el turno \(selectedShift.shiftName) del \(record.recordDate.asFormattedDateString())?")
                }
            }
            .navigationTitle("Editar turno")
            .toolbar {
                ToolbarItem(placement: .cancellationAction) {
                    Button(role: .cancel) {
                        dismiss()
                    } label: {
                        Text("Cerrar")
                    }
                }
                
                ToolbarItem(placement: .confirmationAction) {
                    Button("Guardar") {
                        updateRecord()
                    }
                }
            }
        }
    }
}

extension EditRecordView {
    private func updateRecord() {
        record.date = date
        record.extraTime = Int16(extraTime)
        record.isAllowance = isAllowance
        record.isFreeLicense = isLicense && licenseType == .free
        record.isPaidLicense = isLicense && licenseType == .paid
        record.isWorkedHoliday = isWorkedHoliday
        record.isSpecialWorkedHoliday = isSpecialWorkedHoliday
        record.isMentoring = isMentoring
        record.isSickLeave = isSickLeave
        record.isSPP = isSPP
        record.shift = selectedShift
        record.save()
        dismiss()
    }
    
    private func deleteRecord() {
        record.delete()
        dismiss()
    }
    
    private func refreshShifts() {
        let fetchRequest: NSFetchRequest<ShiftsGroupDate> = ShiftsGroupDate.all
        
        do {
            let allShiftsGroupDates = try viewContext.fetch(fetchRequest)
            
            var currentGroup: ShiftsGroupDate? = nil
            
            for group in allShiftsGroupDates.reversed() {
                guard let validFrom = group.validFrom else { return }
                if validFrom < date || Calendar.current.isDate(validFrom, equalTo: date, toGranularity: .day) {
                    currentGroup = group
                }
            }
            
            let selectedShiftName = selectedShift.shiftName
            self.shifts = currentGroup!.dateShifts
            self.selectedShift = currentGroup!.dateShifts[0]
            
            for shift in shifts {
                if shift.shiftName == selectedShiftName {
                    self.selectedShift = shift
                }
            }
        } catch {
            fatalError("Error while fetching current shifts")
        }
    }
}

