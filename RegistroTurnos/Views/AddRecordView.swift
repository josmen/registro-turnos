//
//  AddRecordView.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 22/11/22.
//

import CoreData
import SwiftUI

struct AddRecordView: View {
    @Environment(\.managedObjectContext) private var viewContext
    
    @Binding var isShowingAddRecord: Bool
    
    @State private var shifts: [Shift]
    @State private var selectedShift: Shift
    @State private var date: Date
    
    init(moc: NSManagedObjectContext, date: Date, isShowingAddRecord: Binding<Bool>) {
        self._isShowingAddRecord = isShowingAddRecord
        self._date = State(wrappedValue: date)
        let fetchRequest: NSFetchRequest<ShiftsGroupDate> = ShiftsGroupDate.all
        
        do {
            let allShiftsGroupDates = try moc.fetch(fetchRequest)
            var currentGroup: ShiftsGroupDate? = nil
            
            for group in allShiftsGroupDates.reversed() {
                if group.validFrom! < date || Calendar.current.isDate(group.validFrom!, equalTo: date, toGranularity: .day) {
                    currentGroup = group
                }
            }
            
            self._shifts = State(initialValue: currentGroup!.dateShifts)
            self._selectedShift = State(initialValue: currentGroup!.dateShifts[0])
        } catch {
            fatalError("Error while fetching current shifts")
        }
    }
    
    var body: some View {
        HStack {
            Picker("Turno", selection: $selectedShift) {
                ForEach(shifts, id: \.self) { shift in
                    Text(shift.shiftName)
                        .tag(shift)
                }
            }
            Spacer()
            DatePicker("Fecha", selection: $date, displayedComponents: .date)
                .labelsHidden()
                .onChange(of: date) {
                    refreshShifts()
                }
            Spacer()
            Button("Guardar") {
                withAnimation {
                    saveRecord()
                    isShowingAddRecord = false
                }
            }
        }        
    }
}

extension AddRecordView {
    private func saveRecord() {
        let record = Record(context: viewContext)
        record.date = date
        record.extraTime = 0
        selectedShift.addToRecords(record)
        selectedShift.save()
    }
    
    private func refreshShifts() {
        let fetchRequest: NSFetchRequest<ShiftsGroupDate> = ShiftsGroupDate.all
        
        do {
            let allShiftsGroupDates = try viewContext.fetch(fetchRequest)
            var currentGroup: ShiftsGroupDate? = nil
            
            for group in allShiftsGroupDates.reversed() {
                guard let validFrom = group.validFrom else { return }
                if validFrom <= date {
                    currentGroup = group
                }
            }
            
            let selectedShiftName = selectedShift.shiftName
            self.shifts = currentGroup!.dateShifts
            self.selectedShift = currentGroup!.dateShifts[0]
            
            for shift in shifts {
                if shift.shiftName == selectedShiftName {
                    self.selectedShift = shift
                }
            }
        } catch {
            fatalError("Error while fetching current shifts")
        }
    }
}
