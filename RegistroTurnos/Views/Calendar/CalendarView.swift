//
//  CalendarView.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 1/12/22.
//

import SwiftUI

struct CalendarView: View {
    @Binding var currentDate: Date
    @Binding var currentMonth: Int
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(spacing: 20) {
                CustomDatePicker(currentDate: $currentDate, currentMonth: $currentMonth)
            }
            .padding(.vertical)
        }
    }
}

struct CalendarView_Previews: PreviewProvider {
    static var previews: some View {
        CalendarView(currentDate: .constant(.now), currentMonth: .constant(1))
    }
}
