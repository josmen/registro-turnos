//
//  CustomDatePicker.swift
//  ElegantTaskApp
//
//  Created by Jose Antonio Mendoza on 2/12/22.
//

import SwiftUI

struct CustomDatePicker: View {
    @Binding var currentDate: Date
    @Binding var currentMonth: Int
    
    @State private var isShowingAddRecord: Bool = false
    @State private var isShowingEditRecord: Bool = false
    @State private var scale = 1.0
    
    @FetchRequest(fetchRequest: Record.all)
    private var records: FetchedResults<Record>
    
    var body: some View {
        VStack(spacing: 35) {
            // Header
            MonthYearHeader(currentDate: $currentDate, currentMonth: $currentMonth)
            
            // Day View
            weekDaysRow()
            
            // Dates - lazy grid
            daysGrid()
            
            // Footer
            recordsFooter()
        }
        .onChange(of: currentMonth) {
            isShowingAddRecord = false
            currentDate = getCurrentMonth()
        }
    }
}

extension CustomDatePicker {
    @ViewBuilder
    func weekDaysRow() -> some View {
        HStack(spacing: 0) {
            let days: [String] = ["Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"]
            ForEach(days, id: \.self) { day in
                Text(day)
                    .font(.caption2)
                    .textCase(.uppercase)
                    .frame(maxWidth: .infinity)
            }
        }
    }
    
    @ViewBuilder
    func daysGrid() -> some View {
        let columns = Array(repeating: GridItem(.flexible()), count: 7)
        
        LazyVGrid(columns: columns, spacing: 8) {
            ForEach(extractDate()) { value in
                CardView(value: value)
                    .background(
                        Capsule()
                            .fill(.appPurple)
                            .padding(.horizontal, 8)
                            .opacity(isSameDay(date1: value.date, date2: currentDate) ? 1 : 0)
                    )
                    .onTapGesture {
                        currentDate = value.date
                    }
            }
        }
    }
    
    @ViewBuilder
    func CardView(value: DateValue) -> some View {
        VStack {
            if value.day != -1 {
                if let record = records.first(where: { record in
                    return isSameDay(date1: record.recordDate, date2: value.date)
                }) {
                    Text("\(value.day)")
                        .font(.callout)
                        .fontWeight(isSameDay(date1: record.recordDate, date2: currentDate) ? .bold : .regular)
                        .foregroundColor(isSameDay(date1: record.recordDate, date2: currentDate) ? .white : .primary)
                        .frame(maxWidth: .infinity)
                    
                    Circle()
                        .fill(record.shift?.shiftColor ?? .appOrange)
                        .frame(width: 8, height: 8)
                } else {
                    Text("\(value.day)")
                        .font(.callout)
                        .fontWeight(isSameDay(date1: value.date, date2: currentDate) ? .bold : .regular)
                        .foregroundColor(isSameDay(date1: value.date, date2: currentDate) ? .white : .primary)
                        .frame(maxWidth: .infinity)
                }
            }
        }
        .padding(.vertical, 8)
        .frame(height: 56, alignment: .top)
    }
    
    @ViewBuilder
    func recordsFooter() -> some View {
        let record = records.first(where: { record in
            return isSameDay(date1: record.recordDate, date2: currentDate)
        })
        
        VStack(spacing: 15) {
            HStack {
                Text("Registros")
                    .font(.title2.bold())
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.vertical, 10)
                if record == nil {
                    Spacer()
                    ButtonView(icon: isShowingAddRecord ? "multiply.circle" : "plus.circle") {
                        isShowingAddRecord.toggle()
                    }
                }
            }
            
            if let record {
                recordRow(record: record)
                    .onLongPressGesture {
                        haptic(.success)
                        isShowingEditRecord.toggle()
                    }
                    .sheet(isPresented: $isShowingEditRecord) {
                        EditRecordView(moc: Record.viewContext, record: record)
                    }
            } else {
                if !isShowingAddRecord {
                    Text("No hay registro en este día.")
                } else {
                    AddRecordView(moc: Record.viewContext, date: currentDate, isShowingAddRecord: $isShowingAddRecord)
                }
            }
        }
        .padding()
    }
    
    @ViewBuilder
    func recordRow(record: Record) -> some View {
        VStack(alignment: .leading, spacing: 10) {
            Text("Turno \(record.shift?.shiftName ?? "")")
                .font(.headline)
                .fontWeight(.black)
            HStack {
                Text("Desde las \(record.shift?.shiftStartTime ?? "") hasta las \(record.shift?.shiftEndTime ?? "")")
                if record.extraTime > 0 {
                    Text("+ \(record.extraTime) minutos de exceso.")
                }
            }
            .font(.caption)
        }
        .padding(.vertical, 10)
        .padding(.horizontal)
        .frame(maxWidth: .infinity, alignment: .leading)
        .background(Color(.appPurple).opacity(0.8).cornerRadius(10))
    }
}

extension CustomDatePicker {
    private func isSameDay(date1: Date, date2: Date) -> Bool {
        let calendar = Calendar.current
        return calendar.isDate(date1, inSameDayAs: date2)
    }
    
    private func getCurrentMonth() -> Date {
        let calendar = Calendar.current
        
        guard let currentMonth = calendar.date(byAdding: .month, value: self.currentMonth, to: Date()) else {
            return Date()
        }
        
        return currentMonth
    }
    
    private func extractDate() -> [DateValue] {
        let calendar = Calendar.current
        
        let currentMonth = getCurrentMonth()
        
        var days = currentMonth.getAllDates().compactMap { date -> DateValue in
            let day = calendar.component(.day, from: date)
            return DateValue(day: day, date: date)
        }
        
        var firstWeekday = calendar.component(.weekday, from: days.first?.date ?? Date())
        if firstWeekday == 1 { firstWeekday += 7 }
        
        for _ in 1..<firstWeekday - 1 {
            days.insert(DateValue(day: -1, date: Date()), at: 0)
        }
        
        return days
    }
}
