//
//  ButtonView.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 21/12/22.
//

import SwiftUI

struct ButtonView: View {
    let icon: String
    let label: String?
    let action: () -> Void
    
    init(icon: String, label: String? = nil, action: @escaping () -> Void) {
        self.icon = icon
        self.label = label
        self.action = action
    }
    
    var body: some View {
        Button {
            action()
        } label: {
            Image(systemName: icon)
            if let label {
                Text(label)
            }
        }
        .font(.title2)
        .buttonStyle(.borderless)
    }
}

struct ButtonView_Previews: PreviewProvider {
    static var previews: some View {
        ButtonView(icon: "plus.circle", label: "Añadir turno") {}
            .previewLayout(.sizeThatFits)
    }
}
