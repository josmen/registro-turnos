//
//  MonthYearHeader.swift
//  RegistroTurnos
//
//  Created by Jose Antonio Mendoza on 5/3/24.
//

import SwiftUI

struct MonthYearHeader: View {
    @Binding var currentDate: Date
    @Binding var currentMonth: Int

    var body: some View {
        HStack(spacing: 20) {
            VStack(alignment: .leading, spacing: 10) {
                Text(currentDate.format("MMMM").capitalized).font(.title.bold()) + Text(" de \(currentDate.format("yyyy"))").font(.title).foregroundColor(.appPurple)
            }
            Spacer(minLength: 0)
            ButtonView(icon: "chevron.left") {
                withAnimation {
                    currentMonth -= 1
                }
            }

            ButtonView(icon: "chevron.right") {
                withAnimation {
                    currentMonth += 1
                }
            }
        }
        .padding(.horizontal)
    }
}

#Preview {
    MonthYearHeader(currentDate: .constant(.now), currentMonth: .constant(3))
}
